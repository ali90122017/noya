<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function arcNewewOrders()
    {
        $orders = Order::where('seen', 0)->get();

        foreach($orders as $order) {
            $order->update([
                'seen' => 1
            ]);
        }

        return response([
            'status' => 'ok',
            'orders' => $orders
        ]);
    }

    public function palizNewewOrders()
    {
        // store request from paliz in orders and order_items tables

        
        return response([
            'status' => 'ok'
        ]);
    }
}
