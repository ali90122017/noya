<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            ['barcode' => '6260079212146', 'name' => 'ماءالشعير دلستر 330 سي سي بهنوش', 'purchase_price' => 78000, 'sale_price' => 90000, 'stock_items' => 12, 'discount' => 2],
            ['barcode' => '6268714601287', 'name' => 'شیر سنتی پرچرب مزرعه ماهشام (220 میل)', 'purchase_price' => 65000, 'sale_price' => 72000, 'stock_items' => 25, 'discount' => 3],
            ['barcode' => '6268714601256', 'name' => 'ماست پرچرب 1000گرم ماهشام', 'purchase_price' => 122000, 'sale_price' => 13000, 'stock_items' => 5, 'discount' => 1],
            ['barcode' => '6264078403628', 'name' => 'پنیر سفید نسبتا چرب 400 گرمی دومینو', 'purchase_price' => 141000, 'sale_price' => 155000, 'stock_items' => 8, 'discount' => 4],
            ['barcode' => '6260190900557', 'name' => 'پودر گرانول سوخاری پولکی (پانکو طلایی) تردک 200 گرمی', 'purchase_price' => 188000, 'sale_price' => 200000, 'stock_items' => 35, 'discount' => 9],
        ];

        foreach($products as $product) {
            $items[] = [
                'barcode' => $product['barcode'],
                'name' => $product['name'],
                'purchase_price' => $product['purchase_price'],
                'sale_price' => $product['sale_price'],
                'stock_items' => $product['stock_items'],
                'discount' => $product['discount'],
                'barcode' => $product['barcode'],
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        Product::insert($items);
    }
}
