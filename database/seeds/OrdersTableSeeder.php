<?php

use App\Order;
use App\Product;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $orders = [
            ['tracking_code' => 2233, 'total_price' => 125600, 'final_price' => 115600, 'payment_type' => 'cash', 'discount' => 10000],
            ['tracking_code' => 7744, 'total_price' => 1400000, 'final_price' => 1300000, 'payment_type' => 'cash', 'discount' => 100000],
            ['tracking_code' => 5858, 'total_price' => 750000, 'final_price' => 700000, 'payment_type' => 'cash', 'discount' => 50000],
            ['tracking_code' => 3666, 'total_price' => 820000, 'final_price' => 780000, 'payment_type' => 'cash', 'discount' => 40000],
            ['tracking_code' => 2558, 'total_price' => 480000, 'final_price' => 430000, 'payment_type' => 'cash', 'discount' => 50000],
            
        ];

        foreach($orders as $order) {
            $items[] = [
                'tracking_code' => $order['tracking_code'],
                'total_price' => $order['total_price'],
                'final_price' => $order['final_price'],
                'payment_type' => $order['payment_type'],
                'discount' => $order['discount'],
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        Order::insert($items);
    }
}
