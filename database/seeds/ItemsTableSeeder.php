<?php

use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Order::pluck('id')->toArray();
        $products = Product::pluck('id')->toArray();

        for ($i=1; $i < 20; $i++) { 
            $items[] = [
                'order_id' => Arr::random($orders), 
                'product_id' => Arr::random($products), 
                'quantity' => rand(2, 8),
                'price' => Arr::random(range(125000, 500000, 1000)),
                'discount' => Arr::random([2, 3, 5, 6]),
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        OrderItem::insert($items);
    }
}
