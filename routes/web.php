<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('/api/v1')->namespace('Api')->name('orders.')->group(function () {
    // Route::get('/orders', function() {
    //     return 'ali';
    // });
    Route::get('/arc-new-orders', 'OrderController@arcNewewOrders')->name('arcNewewOrders');
    Route::match(['get', 'post'], '/paliz-new-orders', 'OrderController@palizNewewOrders')->name('palizNewewOrders');
    
});


